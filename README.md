# BNCC Workshop - Spring Boot REST API

This project is a simple Spring Boot application that provides a REST API for managing products. It uses H2 as the in-memory database and Lombok to reduce boilerplate code. The `Product` entity is identified by a UUID.

## Prerequisites

- Java 22

## Getting Started

### Clone the Repository

```bash
git clone https://github.com/your-username/product-management-api.git
cd product-management-api
```

### Build the Project

TODO: Add instructions for building the project via IntelliJ

```bash
mvn clean install
```

### Run the Application

```bash
mvn spring-boot:run
```

The application will start on `http://localhost:8080`.

## H2 Database Console

To access the H2 database console, navigate to `http://localhost:8080/h2-console` in your browser. Use the following settings to connect:

- **JDBC URL:** `jdbc:h2:mem:testdb`
- **User Name:** `sa`
- **Password:** (leave blank or use `password` if set in `application.properties`)

## API Endpoints

The following endpoints are available:

- **GET** `/api/products` - Retrieve all products
- **GET** `/api/products/{id}` - Retrieve a product by ID
- **POST** `/api/products` - Create a new product
- **PUT** `/api/products/{id}` - Update an existing product
- **DELETE** `/api/products/{id}` - Delete a product

## Example JSON Payloads

### Create a Product

```json
{
  "name": "Sample Product",
  "description": "This is a sample product",
  "price": 29.99
}
```

## Project Structure

- **src/main/java/com/dkatalis/bncc_demo**
    - **controller** - REST controllers
    - **model** - JPA entity models
    - **repository** - Spring Data JPA repositories
    - **service** - Service layer

## Using Lombok

This project uses Lombok to reduce boilerplate code. Ensure your IDE is set up to handle Lombok annotations. You can find the setup instructions for [IntelliJ IDEA](https://projectlombok.org/setup/intellij) on the Lombok website.

## Viewing Table Schema and Data

To view the schema and data of the `Product` table in the H2 console, run the following SQL commands:

```sql
-- View table schema
SHOW COLUMNS FROM Product;

-- View table data
SELECT * FROM Product;
```

## License

This project is licensed under the MIT License. See the [LICENSE](LICENSE) file for details.

## Acknowledgements

- [Spring Boot](https://spring.io/projects/spring-boot)
- [H2 Database](https://www.h2database.com/)
- [Lombok](https://projectlombok.org/)

---

For any issues or questions, feel free to open an issue or contact the repository owner.
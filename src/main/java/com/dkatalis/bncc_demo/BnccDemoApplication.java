package com.dkatalis.bncc_demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BnccDemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(BnccDemoApplication.class, args);
	}

}

package com.dkatalis.bncc_demo.service;

import com.dkatalis.bncc_demo.model.Product;
import com.dkatalis.bncc_demo.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class ProductService {

    @Autowired
    private ProductRepository productRepository;

    public List<Product> getAllProducts() {
        return productRepository.findAll();
    }

    public Optional<Product> getProductById(UUID id) {
        // TODO: Implement this method
        throw new UnsupportedOperationException("Method not implemented yet");
    }

    public Product saveProduct(Product product) {
        return productRepository.save(product);
    }

    public void deleteProduct(UUID id) {
        // TODO: Implement this method
        throw new UnsupportedOperationException("Method not implemented yet");
    }
}
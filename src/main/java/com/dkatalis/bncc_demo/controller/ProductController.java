package com.dkatalis.bncc_demo.controller;

import com.dkatalis.bncc_demo.model.Product;
import com.dkatalis.bncc_demo.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/api/products")
public class ProductController {

    @Autowired
    private ProductService productService;

    @GetMapping
    public List<Product> getAllProducts() {
        return productService.getAllProducts();
    }

    @GetMapping("/{id}")
    public ResponseEntity<Product> getProductById(@PathVariable UUID id) {
        // TODO: Implement this method
        throw new UnsupportedOperationException("Method not implemented yet");
    }

    @PostMapping
    public Product createProduct(@RequestBody Product product) {
        return productService.saveProduct(product);
    }

    @PutMapping("/{id}")
    public ResponseEntity<Product> updateProduct(@PathVariable UUID id, @RequestBody Product product) {
        // TODO: Implement this method
        throw new UnsupportedOperationException("Method not implemented yet");
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteProduct(@PathVariable UUID id) {
        // TODO: Implement this method
        throw new UnsupportedOperationException("Method not implemented yet");
    }
}